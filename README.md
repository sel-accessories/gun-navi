# Gun NAVI

This is an attempt to recreate the Taro's gun-shaped `NAVI` from `Serial Experiments Lain`.
After the 3D modeling the implementation with a `Raspberry Pi Zero 2 W` (or another small form-factor SBC) is intended.

## Work in progress!

The progress is very, very slow, this will take 2 or 3 months until the first prototype. =0

![Assembly](images/ass.png)

## Background

There are two project that recreate some NAVIs from SEL. This is the state of the art.

* [Handy NAVI](https://www.youtube.com/watch?v=9xl2ocTrZM8) from `kuro guro`
* [CHIBI NAVI](https://sketchfab.com/3d-models/chibi-navi-serial-experiments-lain-1d1ac2fccbec428a8e59bb516f400de3) from `Sixthclone`

## Reference image

First sketch to see the possible dimensions.
<p>
<details>
<summary>Click this to collapse/fold.</summary>

<img src="images/1.jpg" alt="First sketch x1.1" width="500"/>

</details>
</p>


## Parts

<p>
<details>
<summary>Click this to collapse/fold.</summary>

### Main body

<img src="images/parts/body.jpg" alt="Main body" width="300"/>

### Screen cover

<img src="images/parts/screen.jpg" alt="Screen cover" width="300"/>

### Front cover

<img src="images/parts/front.jpg" alt="Front cover" width="300"/>

### Back cover

<img src="images/parts/back.jpg" alt="Back cover" width="300"/>

### Trigger

<img src="images/parts/trigger.jpg" alt="Trigger" width="300"/>

### Lost piece

<img src="images/parts/lost_piece.png" alt="Lost piece" width="300"/>

</details>
</p>


## Files

The project consist on an assembly file and several parts files.

You will need:

* ~~[FreeCAD Link Branch](https://github.com/realthunder/FreeCAD_assembly3) from realthunder <3. I'm not sure if it is compatible with FreeCAD 0.19, in the future I plan to port the project to FreeCAD 0.20.~~
* [FreeCAD 0.20](https://github.com/FreeCAD/FreeCAD-Bundle/releases/tag/weekly-builds) as CAD parametric modeler.
* [Part Design Workbench](https://wiki.freecadweb.org/PartDesign_Workbench) for the modeling.
* [A2plus Workbench](https://wiki.freecadweb.org/A2plus_Workbench) for the assembly.
* [Fastener Workbench](https://wiki.freecadweb.org/Fasteners_Workbench) for some screws and nuts.


## External resources


| Resource                        | Author                                    |       License        |                    Link                    |
| :------------------------------ | :---------------------------------------- | :------------------: | :----------------------------------------: |
| Microswitch_SPDT_Vertical       | [hasecilu](https://github.com/hasecilu/)  | Creative Commons 3.0 | [FreeCAD-library](https://cutt.ly/2In3QuN) |
| 3.5 RPi TFT display spi 480x320 | [hasecilu](https://github.com/hasecilu/)  | Creative Commons 3.0 | [FreeCAD-library](https://cutt.ly/FIn3Zqs) |
| Raspberry Pi 4 Model B 3D model | [Hasanain Shuja](https://cutt.ly/LRTxTIj) |          -           |     [GRABCAD](https://cutt.ly/TRTxY1Q)     |
| Raspberry Pi Zero 2 W 3D model  | [Hasanain Shuja](https://cutt.ly/LRTxTIj) |          -           |     [GRABCAD](https://cutt.ly/ETU33NP)     |
| Raspberry Pi Zero W 3D model    | [Julian](https://cutt.ly/gRa9lPW)         |          -           |     [GRABCAD](https://cutt.ly/oRa9m7S)     |

